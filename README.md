# **Distributed Systems Travel Manager** #
###  ###
The purpose of this application is to make travel management allowing two distinct types of clients, the driver and passenger, and a server that manages the various requests from clients.

Passenger type clients make requests to request a travel, while the driver type clients announce availability. The server gives each passenger a driver using for it the distance of Manhattan to find out which driver is closer to each passenger whenever attaches a trip.

In the case where the number of available drivers is less than the number of passengers waiting, the drivers serve passengers in chronological order (FIFO).

The application also allows clients to register and login in before applying for travel or announce availability.